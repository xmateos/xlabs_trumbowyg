<?php

namespace XLabs\TrumboWYGBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_trumbowyg');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->scalarNode('giphy_api_key')->defaultValue('')->end()
                /*->scalarNode('forum_admin_index')->defaultValue('/admin/index')->end()
                ->scalarNode('forum_admin_load_results')->defaultValue('/admin/load/results')->end()
                ->scalarNode('forum_index')->defaultValue('/index')->end()
                ->scalarNode('topic_index')->defaultValue('/topic')->end()
                ->scalarNode('thread_index')->defaultValue('/thread')->end()
                ->scalarNode('user_entity')->isRequired()->end()
                ->scalarNode('admin_user')->isRequired()->end()
                ->booleanNode('use_emojis')->defaultTrue()->end()
                ->arrayNode('uploads')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('folder')->isRequired()->end()
                        // no need to allow extensions; form constraint already expect an image
                        //->arrayNode('allowed_extensions')->prototype('scalar')->defaultValue('[\'jpg\', \'jpeg\']')->end()->end()
                        ->scalarNode('max_file_size')->defaultValue('1M')->end()
                    ->end()
                ->end()*/
            ->end()
        ;

        return $treeBuilder;
    }
}
