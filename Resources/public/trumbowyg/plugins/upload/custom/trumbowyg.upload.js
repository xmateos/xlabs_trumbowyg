/* ===========================================================
 * trumbowyg.upload.js v1.2
 * Upload plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Alexandre Demode (Alex-D)
 *          Twitter : @AlexandreDemode
 *          Website : alex-d.fr
 * Mod by : Aleksandr-ru
 *          Twitter : @Aleksandr_ru
 *          Website : aleksandr.ru
 */

(function ($) {
    'use strict';

    var defaultOptions = {
        serverPath: '',
        serverPath_delete: '',
        fileFieldName: 'fileToUpload',
        data: [],                       // Additional data for ajax [{name: 'key', value: 'value'}]
        headers: {},                    // Additional headers
        xhrFields: {},                  // Additional fields
        urlPropertyName: 'file',        // How to get url from the json response (for instance 'url' for {url: ....})
        statusPropertyName: 'success',  // How to get status from the json response
        success: undefined,             // Success callback: function (data, trumbowyg, $modal, values) {}
        error: undefined,               // Error callback: function () {}
        imageWidthModalEdit: false      // Add ability to edit image width
    };

    var _trumbowyg_custom_ui = {
        trumbowyg: false, // original trumbowyg instance
        modal: false, // original trumbowyg modal
        controls: {
            container: false,
            title: false,
            images: false,
        },
        uploads: [], // array of uploaded images
        init: function(trumbowyg, modal){
            let _this = this;
            _this.trumbowyg = trumbowyg;
            _this.modal = modal;

            _this.controls.container = $('<div/>').addClass('trumbowyg-ui');
            _this.controls.images = $('<div/>').addClass('trumbowyg-ui-images');
            _this.controls.title = $('<div/>').addClass('trumbowyg-ui-title');
            _this.controls.title.html('<h3>Uploaded images</h3');
            _this.controls.container.prepend(_this.controls.title);

            // adjust modal
            _this.modal.find('.trumbowyg-modal-button.trumbowyg-modal-submit').hide();
            _this.modal.find('.trumbowyg-modal-button.trumbowyg-modal-reset').text('Close');
            _this.modal.find('.trumbowyg-modal-title').html('Upload images');

            $.each(_this.uploads, function(i, v){
                _this.addUploadedImage(v);
            });
            _this.controls.container.append(_this.controls.images);
            _this.modal.find('.trumbowyg-modal-box').append(_this.controls.container);

            _this.checkVisibilityStatus();

            _this.modal.on('mouseenter mouseover', '.trumbowyg-ui-item', function(){
                $(this).find('.trumbowyg-ui-item-image').css('transform', 'scale(1.05)');
                $(this).find('.trumbowyg-ui-item-options').css({'opacity': 1});
            }).on('mouseout mouseleave', '.trumbowyg-ui-item', function(){
                $(this).find('.trumbowyg-ui-item-image').css('transform', 'scale(1)');
                $(this).find('.trumbowyg-ui-item-options').css({'opacity': 0});
            }).on('click', '.trumbowyg-ui-item-add', function(e){
                e.preventDefault();
                _this.trumbowyg.execCmd('insertImage', $(this).attr('data-src'), false, true);
                _this.trumbowyg.closeModal();
            }).on('click', '.trumbowyg-ui-item-remove', function(e){
                e.preventDefault();
                let but = $(this);
                $.ajax({
                    url: _this.trumbowyg.o.plugins.upload.serverPath_delete,
                    method: 'POST',
                    dataType: 'json',
                    data: { imagesToDelete: JSON.stringify([but.attr('data-src')]) },
                    success: function(response) {
                        if(response.success)
                        {
                            let matches = $.grep(response.images, function(item) {
                                return but.attr('data-src').includes(item);
                            });
                            if(matches.length)
                            {
                                _this.trumbowyg.$c.trumbowyg('html');
                                let $tmp = $('<div>').html(_this.trumbowyg.$c.trumbowyg('html'));
                                $tmp.find('img[src="' + but.attr('data-src') + '"]').remove();
                                _this.trumbowyg.$c.trumbowyg('html', $tmp.html());
                                _this.uploads = _this.uploads.filter(function(value){
                                    return value !== but.attr('data-src');
                                });
                                if(!_this.uploads.length)
                                {
                                    _this.controls.container.css({'opacity': 0})
                                }
                                but.closest('.trumbowyg-ui-item').remove();
                            }
                        }
                    },
                    error: function(err) {
                        console.error("Error deleting images:", err);
                    }
                });
            });
        },
        addUploadedImage: function(src){
            let _this = this;
            let item = $('<div/>').addClass('trumbowyg-ui-item');
            let options = $('<div/>').addClass('trumbowyg-ui-item-options');
            let cmd_add_image = $('<a/>').addClass('trumbowyg-ui-item-add').attr({
                'href': 'javascript:;',
                'title': 'Add to editor',
                'data-src': src,
            });
            let cmd_delete_image = $('<a/>').addClass('trumbowyg-ui-item-remove').attr({
                'href': 'javascript:;',
                'title': 'Delete uploaded image',
                'data-src': src,
            });
            //let cmd_add_image = $('<a href="javascript:;" class="trumbowyg-ui-item-add" title="Add to editor" data-src="' + src + '"></a>');
            //let cmd_delete_image = $('<a href="javascript:;" class="trumbowyg-ui-item-remove" title="Delete uploaded image" data-src="' + src + '"></a>');
            let img = $('<img/>').addClass('trumbowyg-ui-item-image').attr({
                'src': src
            });
            options
                .append(cmd_add_image)
                .append(cmd_delete_image)
            ;
            item
                .append(options)
                .append(img)
            ;
            _this.controls.images.append(item);
        },
        checkVisibilityStatus: function(){
            let _this = this;
            if(_this.uploads.length)
            {
                _this.controls.container.css({'opacity': 1});
            } else {
                _this.controls.container.css({'opacity': 0});
            }
            // Adjust modal height
            let contentHeight = _this.modal.find('.trumbowyg-modal-box').outerHeight();
            _this.modal.height(contentHeight);
        }
    };

    function getDeep(object, propertyParts) {
        var mainProperty = propertyParts.shift(),
            otherProperties = propertyParts;

        if (object !== null) {
            if (otherProperties.length === 0) {
                return object[mainProperty];
            }

            if (typeof object === 'object') {
                return getDeep(object[mainProperty], otherProperties);
            }
        }
        return object;
    }

    addXhrProgressEvent();

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            en: {
                upload: 'Upload',
                file: 'File',
                uploadError: 'Error'
            },
            az: {
                upload: 'Yüklə',
                file: 'Fayl',
                uploadError: 'Xəta'
            },
            by: {
                upload: 'Загрузка',
                file: 'Файл',
                uploadError: 'Памылка'
            },
            ca: {
                upload: 'Pujar fitxer',
                file: 'Fitxer',
                uploadError: 'Error'
            },
            cs: {
                upload: 'Nahrát obrázek',
                file: 'Soubor',
                uploadError: 'Chyba'
            },
            da: {
                upload: 'Upload',
                file: 'Fil',
                uploadError: 'Fejl'
            },
            de: {
                upload: 'Hochladen',
                file: 'Datei',
                uploadError: 'Fehler'
            },
            es: {
                upload: 'Subir archivo',
                file: 'Archivo',
                uploadError: 'Error'
            },
            et: {
                upload: 'Lae üles',
                file: 'Fail',
                uploadError: 'Viga'
            },
            fr: {
                upload: 'Envoi',
                file: 'Fichier',
                uploadError: 'Erreur'
            },
            hu: {
                upload: 'Feltöltés',
                file: 'Fájl',
                uploadError: 'Hiba'
            },
            ja: {
                upload: 'アップロード',
                file: 'ファイル',
                uploadError: 'エラー'
            },
            ko: {
                upload: '그림 올리기',
                file: '파일',
                uploadError: '에러'
            },
            pt_br: {
                upload: 'Enviar do local',
                file: 'Arquivo',
                uploadError: 'Erro'
            },
            ru: {
                upload: 'Загрузка',
                file: 'Файл',
                uploadError: 'Ошибка'
            },
            sl: {
                upload: 'Naloži datoteko',
                file: 'Datoteka',
                uploadError: 'Napaka'
            },
            sk: {
                upload: 'Nahrať',
                file: 'Súbor',
                uploadError: 'Chyba'
            },
            tr: {
                upload: 'Yükle',
                file: 'Dosya',
                uploadError: 'Hata'
            },
            zh_cn: {
                upload: '上传',
                file: '文件',
                uploadError: '错误'
            },
            zh_tw: {
                upload: '上傳',
                file: '文件',
                uploadError: '錯誤'
            },
        },
        // jshint camelcase:true

        plugins: {
            upload: {
                init: function (trumbowyg) {
                    trumbowyg.o.plugins.upload = $.extend(true, {}, defaultOptions, trumbowyg.o.plugins.upload || {});
                    var btnDef = {
                        fn: function () {
                            trumbowyg.saveRange();

                            var file,
                                prefix = trumbowyg.o.prefix;

                            var fields = {
                                file: {
                                    type: 'file',
                                    required: true,
                                    attributes: {
                                        accept: 'image/*'
                                    }
                                },
                                alt: {
                                    label: 'description',
                                    value: trumbowyg.getRangeText()
                                }
                            };

                            if (trumbowyg.o.plugins.upload.imageWidthModalEdit) {
                                fields.width = {
                                    value: ''
                                };
                            }

                            // Prevent multiple submissions while uploading
                            var isUploading = false;

                            var $modal = trumbowyg.openModalInsert(
                                // Title
                                trumbowyg.lang.upload,

                                // Fields
                                fields,

                                // Callback
                                function (values) {
                                    if (isUploading) {
                                        return;
                                    }
                                    isUploading = true;

                                    var data = new FormData();
                                    data.append(trumbowyg.o.plugins.upload.fileFieldName, file);

                                    trumbowyg.o.plugins.upload.data.map(function (cur) {
                                        data.append(cur.name, cur.value);
                                    });

                                    $.map(values, function (curr, key) {
                                        if (key !== 'file') {
                                            data.append(key, curr);
                                        }
                                    });

                                    if ($('.' + prefix + 'progress', $modal).length === 0) {
                                        $('.' + prefix + 'modal-title', $modal)
                                            .after(
                                                $('<div/>', {
                                                    'class': prefix + 'progress'
                                                }).append(
                                                    $('<div/>', {
                                                        'class': prefix + 'progress-bar'
                                                    })
                                                )
                                            );
                                    }

                                    $.ajax({
                                        url: trumbowyg.o.plugins.upload.serverPath,
                                        headers: trumbowyg.o.plugins.upload.headers,
                                        xhrFields: trumbowyg.o.plugins.upload.xhrFields,
                                        type: 'POST',
                                        data: data,
                                        cache: false,
                                        dataType: 'json',
                                        processData: false,
                                        contentType: false,

                                        progressUpload: function (e) {
                                            $('.' + prefix + 'progress-bar').css('width', Math.round(e.loaded * 100 / e.total) + '%');
                                        },

                                        success: function (data) {
                                            if (!!getDeep(data, trumbowyg.o.plugins.upload.statusPropertyName.split('.'))) {
                                                var url = getDeep(data, trumbowyg.o.plugins.upload.urlPropertyName.split('.'));
                                                /*trumbowyg.execCmd('insertImage', url, false, true);
                                                var $img = $('img[src="' + url + '"]:not([alt])', trumbowyg.$box);
                                                $img.attr('alt', values.alt);
                                                if (trumbowyg.o.plugins.upload.imageWidthModalEdit && parseInt(values.width) > 0) {
                                                    $img.attr({
                                                        width: values.width
                                                    });
                                                }
                                                setTimeout(function () {
                                                    trumbowyg.closeModal();
                                                }, 250);*/
                                                trumbowyg.$c.trigger('tbwuploadsuccess', [trumbowyg, data, url]);

                                                _trumbowyg_custom_ui.uploads.push(url);
                                                _trumbowyg_custom_ui.addUploadedImage(url);
                                                _trumbowyg_custom_ui.checkVisibilityStatus();
                                                _trumbowyg_custom_ui.modal.find('input[type="file"]').val('');

                                                // Call custom callback anyways
                                                if(trumbowyg.o.plugins.upload.success)
                                                {
                                                    trumbowyg.o.plugins.upload.success(data, trumbowyg, $modal, values);
                                                }
                                            } else {
                                                trumbowyg.addErrorOnModalField(
                                                    $('input[type=file]', $modal),
                                                    trumbowyg.lang[data.message]
                                                );
                                                trumbowyg.$c.trigger('tbwuploaderror', [trumbowyg, data]);

                                                if(trumbowyg.o.plugins.upload.error)
                                                {
                                                    trumbowyg.o.plugins.upload.error(trumbowyg.lang[data.message]);
                                                }
                                            }

                                            isUploading = false;
                                        },

                                        error: function () {
                                            trumbowyg.addErrorOnModalField(
                                                $('input[type=file]', $modal),
                                                trumbowyg.lang.uploadError
                                            );
                                            trumbowyg.$c.trigger('tbwuploaderror', [trumbowyg]);

                                            isUploading = false;

                                            if(trumbowyg.o.plugins.upload.error)
                                            {
                                                trumbowyg.o.plugins.upload.error(trumbowyg.lang.uploadError);
                                            }
                                        }
                                    });
                                }
                            );

                            $('input[type=file]').on('change', function (e) {
                                try {
                                    // If multiple files allowed, we just get the first.git config --global --add safe.directory /var/www/gamification/src/ATM/InboxBundle
                                    file = e.target.files[0];
                                } catch (err) {
                                    // In IE8, multiple files not allowed
                                    file = e.target.value;
                                }
                                // auto submit
                                _trumbowyg_custom_ui.modal.find('.trumbowyg-modal-button.trumbowyg-modal-submit').click();
                            });

                            _trumbowyg_custom_ui.init(trumbowyg, $modal);
                        }
                    };

                    trumbowyg.addBtnDef('upload', btnDef);
                }
            }
        }
    });

    function addXhrProgressEvent() {
        if (!$.trumbowyg.addedXhrProgressEvent) {   // Avoid adding progress event multiple times
            var originalXhr = $.ajaxSettings.xhr;
            $.ajaxSetup({
                xhr: function () {
                    var that = this,
                        req = originalXhr();

                    if (req && typeof req.upload === 'object' && that.progressUpload !== undefined) {
                        req.upload.addEventListener('progress', function (e) {
                            that.progressUpload(e);
                        }, false);
                    }

                    return req;
                }
            });
            $.trumbowyg.addedXhrProgressEvent = true;
        }
    }
})(jQuery);
