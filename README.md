TrumboWYG plugin wrapper bundle.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/trumbowygundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\TrumboWYGBundle\XLabsTrumboWYGBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
xlabs_trumbowyg_routing:
    resource: "@XLabsTrumboWYGBundle/Resources/config/routing.yml"
```
