<?php

namespace XLabs\TrumboWYGBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use \Exception;
use Symfony\Component\HttpKernel\KernelInterface;

class TrumboWYGController extends Controller
{
    /**
     * @Route("/upload", name="xlabs_trumbowyg_upload", options={"expose"=true})
     */
    public function uploadImageAction(Request $request, KernelInterface $kernel)
    {
        $destination_folder = $request->request->get('folder');
        $destination_filename = $request->request->get('filename') ? $request->request->get('filename') : md5(uniqid()); // without extension
        if(!$destination_folder)
        {
            return new JsonResponse([
                'success' => false
            ]);
        }

        self::createFolder($destination_folder, 0777);
        $file = $request->files->get('file');
        $filename = $file->getClientOriginalName();
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $destination_filename = $destination_filename.'.'.$extension;
        if($file->move($destination_folder, $destination_filename))
        {
            return new JsonResponse([
                'success' => true,
                'file' => $request->getScheme().'://'.$request->getHttpHost().'/'.str_replace($kernel->getRootDir().'/../web/', '', rtrim($destination_folder, '/').'/'.$destination_filename).'?'.time()
            ]);
        }

        return new JsonResponse([
            'success' => false
        ]);
    }

    /**
     * @Route("/upload/delete", name="xlabs_trumbowyg_upload_delete", options={"expose"=true})
     */
    public function deleteAction(Request $request, KernelInterface $kernel)
    {
        $imagesToDelete = $request->request->get('imagesToDelete');
        $aImages = [];
        if($imagesToDelete)
        {
            $web_folder = $kernel->getRootDir().'/../web/';
            foreach(json_decode($imagesToDelete, true) as $image)
            {
                // clean the image ending hash, added for browser cache when upload funcion above returns the image url
                $parsedImageUrl = parse_url($image);
                $cleanImageUrl = $parsedImageUrl['scheme'].'://'.$parsedImageUrl['host'].($parsedImageUrl['path'] ?? '');
                $image = str_replace($request->getScheme().'://'.$request->getHttpHost().'/', '', $cleanImageUrl);
                $aImages[] = $image;
                if(file_exists($web_folder.$image))
                {
                    @unlink($web_folder.$image);
                }
            }
            return new JsonResponse([
                'success' => true,
                'images' => $aImages
            ]);
        }
        return new JsonResponse([
            'success' => false,
            'images' => $aImages
        ]);
    }

    // copy from MMCoreBundle helpers
    public static function createFolder($folder, $mode = 0775)
    {
        $createFolder = true;
        if(!is_dir($folder))
        {
            $oldmask = umask(0);
            try {
                //$createFolder = mkdir($folder, $mode, true);
                $createFolder = @mkdir($folder, $mode, true);
            } catch(Exception $e) {
                dump($e); die;
            }
            umask($oldmask);
        }
        return $createFolder;
    }
}

