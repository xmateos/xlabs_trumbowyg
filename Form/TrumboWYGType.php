<?php

namespace XLabs\TrumboWYGBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\RouterInterface;

class TrumboWYGType extends AbstractType
{
    private $router;
    private $xlabs_trumbowyg_config;

    public function __construct(RouterInterface $router, $xlabs_trumbowyg_config)
    {
        $this->router = $router;
        $this->xlabs_trumbowyg_config = $xlabs_trumbowyg_config;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        /*
         * i.e.    'tbwchange' => 'myFunc'
         * this will call, if exists, function 'myFunc' set in the template, passing always the form element initialized with the plugin
         */
        $defaultCallbacks = [
            'tbwfocus' => false, // focus
            'tbwblur' => false, // blur
            'tbwinit' => false, // init
            'tbwchange' => false, // change
            'tbwresize' => false, // resize
            'tbwpaste' => false, // paste
            'tbwopenfullscreen' => false, // enter fullscreen
            'tbwclosefullscreen' => false, // leave fullscreen
            'tbwclose' => false, // close
            'tbwmodalopen' => false, // modal open
            'tbwmodalclose' => false, // modal close
        ];
        $resolver->setDefaults(array(
            'plugin_options' => [],
            'plugin_events' => $defaultCallbacks,
            'plugins' => []
        ));

        // Override the resolve method
        $resolver->setNormalizer('plugin_events', function(Options $options, $values) use ($defaultCallbacks) {
            return array_replace_recursive($defaultCallbacks, $values);
        });
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $plugin_options = $options['plugin_options'];
        $plugin_events = $options['plugin_events'];
        $plugins = [];
        if(isset($plugin_options['btns']) && (self::valueExists('foreColor', $plugin_options['btns']) || self::valueExists('backColor', $plugin_options['btns'])))
        {
            $plugins[] = 'color';
        }
        if(isset($plugin_options['btns']) && self::valueExists('image', $plugin_options['btns']))
        {
            $plugins[] = 'image';
            // defaults
            if(!isset($plugin_options['btnsDef']))
            {
                $plugin_options['btnsDef'] = [];
            }
            self::custom_array_merge($plugin_options['btnsDef'], [
                'image' => [
                    'dropdown' => ['insertImage', 'base64'],
                    'ico' => 'insertImage'
                ]
            ]);
        }
        if(isset($plugin_options['btns']) && self::valueExists('upload', $plugin_options['btns']))
        {
            $plugins[] = 'upload';
            // defaults
            self::checkPluginDefinition($plugin_options, 'upload');
            self::custom_array_merge($plugin_options['plugins']['upload'], [
                'serverPath' => $this->router->generate('xlabs_trumbowyg_upload'),
                'serverPath_delete' => $this->router->generate('xlabs_trumbowyg_upload_delete'),
                'fileFieldName' => 'file',
                //'urlPropertyName' => 'data.file'
            ]);
            // defaults
            if(!isset($plugin_options['btnsDef']))
            {
                $plugin_options['btnsDef'] = [];
            }
            // Replace upload icon + tooltip
            self::custom_array_merge($plugin_options['btnsDef'], [
                'upload' => [
                    'ico' => 'insertImage',
                    'title' => 'Add image'
                ]
            ]);
        }
        if(isset($plugin_options['btns']) && self::valueExists('emoji', $plugin_options['btns']))
        {
            $plugins[] = 'emoji';
        }
        if(isset($plugin_options['btns']) && self::valueExists('fontfamily', $plugin_options['btns']))
        {
            $plugins[] = 'fontfamily';
        }
        if(isset($plugin_options['btns']) && self::valueExists('fontsize', $plugin_options['btns']))
        {
            $plugins[] = 'fontsize';
        }
        if(isset($plugin_options['btns']) && self::valueExists('giphy', $plugin_options['btns']))
        {
            $plugins[] = 'giphy';
            // defaults
            self::checkPluginDefinition($plugin_options, 'giphy');
            self::custom_array_merge($plugin_options['plugins']['giphy'], [
                'apiKey' => isset($this->xlabs_trumbowyg_config['giphy_api_key']) ?? $this->xlabs_trumbowyg_config['giphy_api_key']
            ]);
        }
        if(isset($plugin_options['btns']) && (self::valueExists('historyUndo', $plugin_options['btns']) || self::valueExists('historyRedo', $plugin_options['btns'])))
        {
            $plugins[] = 'history';
        }
        if(isset($plugin_options['btns']) && (self::valueExists('indent', $plugin_options['btns']) || self::valueExists('outdent', $plugin_options['btns'])))
        {
            $plugins[] = 'indent';
        }
        if(isset($plugin_options['btns']) && self::valueExists('insertAudio', $plugin_options['btns']))
        {
            $plugins[] = 'insertAudio';
        }
        if(isset($plugin_options['btns']) && self::valueExists('lineheight', $plugin_options['btns']))
        {
            $plugins[] = 'lineheight';
        }
        if(isset($plugin_options['btns']) && self::valueExists('noembed', $plugin_options['btns']))
        {
            $plugins[] = 'noembed';
        }
        if(isset($plugin_options['btns']) && self::valueExists('preformatted', $plugin_options['btns']))
        {
            $plugins[] = 'preformatted';
        }
        if(isset($plugin_options['btns']) && self::valueExists('specialChars', $plugin_options['btns']))
        {
            $plugins[] = 'specialChars';
        }
        $view->vars['plugin_options'] = $plugin_options;
        $view->vars['plugin_events'] = $plugin_events;
        $view->vars['plugins'] = $plugins;
    }

    private function checkPluginDefinition(&$config, $plugin)
    {
        if(!array_key_exists('plugins', $config))
        {
            $config['plugins'] = [];
        }
        if(!array_key_exists($plugin, $config['plugins']))
        {
            $config['plugins'][$plugin] = [];
        }
    }

    private function valueExists($needle, $haystack)
    {
        if(is_array($haystack))
        {
            foreach($haystack as $item)
            {
                if(self::valueExists($needle, $item))
                {
                    return true;
                }
            }
        } else {
            if($needle === $haystack)
            {
                return true;
            }
        }
        return false;
    }

    private function custom_array_merge(&$arr1, $arr2)
    {
        foreach($arr2 as $key => $value)
        {
            if(array_key_exists($key, $arr1) && is_array($value))
            {
                $arr1[$key] = self::custom_array_merge($arr1[$key], $arr2[$key]);
            } else {
                if(!isset($arr1[$key]))
                {
                    $arr1[$key] = $value;
                }
            }
        }
        return $arr1;
    }

    public function getParent()
    {
        return TextareaType::class;
    }

    public function getBlockPrefix()
    {
        return 'trumbowyg';
    }
}
